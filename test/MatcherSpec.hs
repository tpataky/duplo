
module MatcherSpec where

import Test.Hspec
import Test.QuickCheck

match :: Double -> [a] -> [a] -> [a]
match t a b = a

distanceMatrix a b = let c0 = take ((length a) + 1) [0..] in
    c0 : (distanceMatrix' a b c0 1)

distanceMatrix' a [ch] cl i = [ i : column cl ch a i ]
distanceMatrix' a (ch : tl) cl i = c : distanceMatrix' a tl c (i + 1) where
    c = i : column cl ch a i

column _ _ [] d0 = [] 
column (c0 : c1 : ct) ch (ch' : str) d0 | ch == ch' = c0 : column (c1 : ct) ch str c0 
column (c0 : c1 : ct) ch (ch' : str) d0             =  d : column (c1 : ct) ch str d
    where
        d = (min d0 (min c0 c1)) + 1 

spec :: Spec
spec = do
    it "should" $ do
        property (\t -> match t [] [] == ([]::[Int]))

    it "should" $ do
        column [0..] 'a' "" 1 `shouldBe` []
        column [0..] 'a' "asdfqwer" 1 `shouldBe` [0..7]
        column [0..] 's' "asdfqwer" 1 `shouldBe` 1 : [1..7]

    it "should" $ do
        distanceMatrix "a" "a" `shouldBe` [[0, 1], [1, 0]]
        distanceMatrix "a" "b" `shouldBe` [[0, 1], [1, 1]]
        distanceMatrix "sitting" "kitten" `shouldBe` [
                [0, 1, 2, 3, 4, 5, 6, 7],
                [1, 1, 2, 3, 4, 5, 6, 7],
                [2, 2, 1, 2, 3, 4, 5, 6],
                [3, 3, 2, 1, 2, 3, 4, 5],
                [4, 4, 3, 2, 1, 2, 3, 4],
                [5, 5, 4, 3, 2, 2, 3, 4],
                [6, 6, 5, 4, 3, 3, 2, 3]
            ]
