
module TokenizerSpec where

import Test.Hspec
import Test.QuickCheck
import Tokenizer
import Language.Java.Lexer
import Data.List (tails)
import Data.Coerce

instance Arbitrary SimpleToken where
    arbitrary = elements [
                    SimpleToken KW_Abstract         
                    ,SimpleToken KW_Assert           
                    ,SimpleToken KW_Boolean          
                    ,SimpleToken KW_Break            
                    ,SimpleToken KW_Byte             
                    ,SimpleToken KW_Case             
                    ,SimpleToken KW_Catch            
                    ,SimpleToken KW_Char             
                    ,SimpleToken KW_Class            
                    ,SimpleToken KW_Const            
                    ,SimpleToken KW_Continue         
                    ,SimpleToken KW_Default          
                    ,SimpleToken KW_Do               
                    ,SimpleToken KW_Double           
                    ,SimpleToken KW_Else             
                    ,SimpleToken KW_Enum             
                    ,SimpleToken KW_Extends          
                    ,SimpleToken KW_Final            
                    ,SimpleToken KW_Finally          
                    ,SimpleToken KW_Float            
                    ,SimpleToken KW_For              
                    ,SimpleToken KW_Goto             
                    ,SimpleToken KW_If               
                    ,SimpleToken KW_Implements       
                    ,SimpleToken KW_Import           
                    ,SimpleToken KW_Instanceof       
                    ,SimpleToken KW_Int              
                    ,SimpleToken KW_Interface        
                    ,SimpleToken KW_Long             
                    ,SimpleToken KW_Native           
                    ,SimpleToken KW_New              
                    ,SimpleToken KW_Package          
                    ,SimpleToken KW_Private          
                    ,SimpleToken KW_Protected        
                    ,SimpleToken KW_Public           
                    ,SimpleToken KW_Return           
                    ,SimpleToken KW_Short            
                    ,SimpleToken KW_Static           
                    ,SimpleToken KW_Strictfp         
                    ,SimpleToken KW_Super            
                    ,SimpleToken KW_Switch           
                    ,SimpleToken KW_Synchronized     
                    ,SimpleToken KW_This             
                    ,SimpleToken KW_Throw            
                    ,SimpleToken KW_Throws           
                    ,SimpleToken KW_Transient        
                    ,SimpleToken KW_Try              
                    ,SimpleToken KW_Void             
                    ,SimpleToken KW_Volatile         
                    ,SimpleToken KW_While            
                    ,SimpleToken OpenParen           
                    ,SimpleToken CloseParen          
                    ,SimpleToken OpenSquare          
                    ,SimpleToken CloseSquare         
                    ,SimpleToken OpenCurly           
                    ,SimpleToken CloseCurly          
                    ,SimpleToken SemiColon           
                    ,SimpleToken Comma               
                    ,SimpleToken Period              
                    ,SimpleToken (IntTok 0)          
                    ,SimpleToken (LongTok 0)         
                    ,SimpleToken (DoubleTok 0.0)       
                    ,SimpleToken (FloatTok 0.0)        
                    ,SimpleToken (CharTok ' ')         
                    ,SimpleToken (StringTok "")       
                    ,SimpleToken (BoolTok False)         
                    ,SimpleToken NullTok             
                    ,SimpleToken (IdentTok "")        
                    ,SimpleToken Op_Equal            
                    ,SimpleToken Op_GThan            
                    ,SimpleToken Op_LThan            
                    ,SimpleToken Op_Bang             
                    ,SimpleToken Op_Tilde            
                    ,SimpleToken Op_Query            
                    ,SimpleToken Op_Colon            
                    ,SimpleToken Op_Equals           
                    ,SimpleToken Op_LThanE           
                    ,SimpleToken Op_GThanE           
                    ,SimpleToken Op_BangE            
                    ,SimpleToken Op_AAnd             
                    ,SimpleToken Op_OOr              
                    ,SimpleToken Op_PPlus            
                    ,SimpleToken Op_MMinus           
                    ,SimpleToken Op_Plus             
                    ,SimpleToken Op_Minus            
                    ,SimpleToken Op_Star             
                    ,SimpleToken Op_Slash            
                    ,SimpleToken Op_And              
                    ,SimpleToken Op_Or               
                    ,SimpleToken Op_Caret            
                    ,SimpleToken Op_Percent          
                    ,SimpleToken Op_LShift           
                    ,SimpleToken Op_PlusE            
                    ,SimpleToken Op_MinusE           
                    ,SimpleToken Op_StarE            
                    ,SimpleToken Op_SlashE           
                    ,SimpleToken Op_AndE             
                    ,SimpleToken Op_OrE              
                    ,SimpleToken Op_CaretE           
                    ,SimpleToken Op_PercentE         
                    ,SimpleToken Op_LShiftE          
                    ,SimpleToken Op_RShiftE          
                    ,SimpleToken Op_RRShiftE         
                    ,SimpleToken Op_AtSign           
                    ]

suffixes :: [L SimpleToken] -> [((Int, Int), [SimpleToken])]
suffixes tokens = zip positions (tails stripped) where
    stripped = map unPos tokens
    positions = map (\(L p _) -> p) tokens

unPos (L _ t) = t

spec :: Spec
spec = do
    it "all identifiers compare equal" $ do
        tokenize "package com;" `shouldBe` coerce [L (1,1) KW_Package,L (1,9) (IdentTok ""),L (1,12) SemiColon]

    it "all literals compare equal" $ do
        let
            tokens = map unPos $ tokenize "1 3L 0.0 0.0f 'c' \"s\" true null"
            allEqual = do
                a <- tokens
                b <- tokens
                return (a == b)
        and allEqual `shouldBe` True

    it "compare equal iff they are equal" $ do
        property comparesEqualIffEqual

    it "should" $ do
        (suffixes $ tokenize "package com; import com.com") `shouldBe` []

comparesEqualIffEqual :: SimpleToken -> SimpleToken -> Bool
comparesEqualIffEqual t1 t2 = if (t1 == t2) then compare t1 t2 == EQ else compare t1 t2 /= EQ


