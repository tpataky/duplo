
module Tokenizer where

import Language.Java.Lexer
import Data.Coerce
import Data.Ord (comparing)

newtype SimpleToken = SimpleToken Token deriving Show

instance Eq SimpleToken where
    (==) a b = isEq (coerce a) (coerce b) where
        isEq (IdentTok _) (IdentTok _)              = True
        isEq t1 t2 | isLiteral t1 && isLiteral t2   = True
        isEq t1 t2                                  = t1 == t2

instance Ord SimpleToken where
    compare = comparing (\t -> toInt $ coerce t)

tokenize :: String -> [L SimpleToken]
tokenize str = coerce (lexer str)

isLiteral :: Token -> Bool
isLiteral (IntTok _)    = True
isLiteral (LongTok _)   = True
isLiteral (DoubleTok _) = True
isLiteral (FloatTok _)  = True
isLiteral (CharTok _)   = True
isLiteral (StringTok _) = True
isLiteral (BoolTok _)   = True
isLiteral NullTok       = True
isLiteral _             = False

toInt :: Token -> Int
toInt KW_Abstract         = 1
toInt KW_Assert           = 2
toInt KW_Boolean          = 3
toInt KW_Break            = 4
toInt KW_Byte             = 5
toInt KW_Case             = 6
toInt KW_Catch            = 7
toInt KW_Char             = 8
toInt KW_Class            = 9
toInt KW_Const            = 10
toInt KW_Continue         = 11
toInt KW_Default          = 12
toInt KW_Do               = 13
toInt KW_Double           = 14
toInt KW_Else             = 15
toInt KW_Enum             = 16
toInt KW_Extends          = 17
toInt KW_Final            = 18
toInt KW_Finally          = 19
toInt KW_Float            = 20
toInt KW_For              = 21
toInt KW_Goto             = 22
toInt KW_If               = 23
toInt KW_Implements       = 24
toInt KW_Import           = 25
toInt KW_Instanceof       = 26
toInt KW_Int              = 27
toInt KW_Interface        = 28
toInt KW_Long             = 29
toInt KW_Native           = 30
toInt KW_New              = 31
toInt KW_Package          = 32
toInt KW_Private          = 33
toInt KW_Protected        = 34
toInt KW_Public           = 35
toInt KW_Return           = 36
toInt KW_Short            = 37
toInt KW_Static           = 38
toInt KW_Strictfp         = 39
toInt KW_Super            = 40
toInt KW_Switch           = 41
toInt KW_Synchronized     = 42
toInt KW_This             = 43
toInt KW_Throw            = 44
toInt KW_Throws           = 45
toInt KW_Transient        = 46
toInt KW_Try              = 47
toInt KW_Void             = 48
toInt KW_Volatile         = 49
toInt KW_While            = 50
toInt OpenParen           = 51
toInt CloseParen          = 52
toInt OpenSquare          = 53
toInt CloseSquare         = 54
toInt OpenCurly           = 55
toInt CloseCurly          = 56
toInt SemiColon           = 57
toInt Comma               = 58
toInt Period              = 59
toInt (IntTok _)          = 60
toInt (LongTok _)         = 60
toInt (DoubleTok _)       = 60
toInt (FloatTok _)        = 60
toInt (CharTok _)         = 60
toInt (StringTok _)       = 60
toInt (BoolTok _)         = 60
toInt NullTok             = 60
toInt (IdentTok _)        = 61
toInt Op_Equal            = 62
toInt Op_GThan            = 63
toInt Op_LThan            = 64
toInt Op_Bang             = 65
toInt Op_Tilde            = 66
toInt Op_Query            = 67
toInt Op_Colon            = 68
toInt Op_Equals           = 69
toInt Op_LThanE           = 70
toInt Op_GThanE           = 71
toInt Op_BangE            = 72
toInt Op_AAnd             = 73
toInt Op_OOr              = 74
toInt Op_PPlus            = 75
toInt Op_MMinus           = 76
toInt Op_Plus             = 77
toInt Op_Minus            = 78
toInt Op_Star             = 79
toInt Op_Slash            = 80
toInt Op_And              = 81
toInt Op_Or               = 82
toInt Op_Caret            = 83
toInt Op_Percent          = 84
toInt Op_LShift           = 85
toInt Op_PlusE            = 86
toInt Op_MinusE           = 87
toInt Op_StarE            = 88
toInt Op_SlashE           = 89
toInt Op_AndE             = 90
toInt Op_OrE              = 91
toInt Op_CaretE           = 92
toInt Op_PercentE         = 93
toInt Op_LShiftE          = 94
toInt Op_RShiftE          = 95
toInt Op_RRShiftE         = 96
toInt Op_AtSign           = 97

